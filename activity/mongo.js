/*
In myFirstDB, do the following:
	1. create a "courses" collection and add the following three courses in one command:
	- Basic HTML
	- Basic CSS
	- Basic JavaScript
	2. in the same command, make sure to specify the course's description, price, isActive, and add an enrollees array.

	For Basic JavaScript ONLY, with an object inside the enrollees array. The object must include a userId field with a value of our only user's Object ID.
*/

db.courses.insertMany([
		{courseName: "Basic HTML", description: "Lorem Ipsum", price: 1000, isActive: true, enrollees: []},
		{courseName: "Basic CSS", description: "Lorem Ipsum", price: 2000, isActive: true, enrollees: []},
		{courseName: "Basic JavaScript", description: "Lorem Ipsum", price: 3000, isActive: false, enrollees: [{_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")}]}
	])