/*
	Using MongoDB locally:
		To use MongoDB on your local machine, you must first open a temrinal and type the command "mongod" to run the MongoDB Community server.
		The server must always be running for the duration of your use of your local MongoDB interface/Mongo Shell.
		Open a separate terminal and type "mongo" to open the Mongo Shell, which is a command line interface a that allows us to issue MongoDB commands.
*/

// MongoDB Command
/*
-Show list of all databases
	- show dbs

-Use <database name> - the command is only the first step in creating a database. Next, you must create a single record for the database to actually be created
	- use myFirstDB

-Create a single document
	- db.users.insertOne({name: "John Smith", age: 20, isActive: true})

-Get a list of all users
	- db.users.find()

-Create a second user
	- db.users.insertOne({name: "Jane Doe", age: 21, isActive: false})

-Within each database is a number of collections, where multiple related data is stored (users, posts, products, etc)
-create a collection - if a collection does not exist yet, MongoDB created the collection when you first store data for that collection
	- db.products.insertMany([
		{name: "Product 1", price: 200.50, stock: 100, description: "Lorem Ipsum"},
		{name: "Product 2", price: 333, stock: 25, description: "Lorem ipsum"}
	])

-find specific document
	- db.products.find({name: "Product 2"})
	- db.users.find({isActive: true})

-Update an existing document:
	- db.users.updateOne()
	- db.users.updateOne(
		{_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")}, 
		{
			$set: {isActive: true}                   
		}
	)

-Update to add a new field:
	- db.users.updateOne(
		{_id: ObjectId("61fbcaa6d5b48a79bc5f15c0")}, 
		{
			$set: {
			addresss: {
				street: "123 Street st",
				city: "New York",
				country: "United States"
					  }
				  }                   
		}
	)

-Format results in a more presentable way add .pretty()
	- db.users.find().pretty()

-Delete a document"
	- db.users.deleteOne({_id: ObjectId("61fbc9d987aff066f05d6b12")})
	- db.users.deleteOne()
*/